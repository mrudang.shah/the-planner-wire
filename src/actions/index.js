export const loadingStart = () => {
    return (dispatch) => {
      dispatch({ type: 'LOADING_START'})
    }
}

export const loadingStop = () => {
    return (dispatch) => {
      dispatch({ type: 'LOADING_DONE'})
    }
}