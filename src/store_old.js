import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers/rootReducer';
import thunk from 'redux-thunk';
import { persistState } from "redux-devtools";
import DevTools from "redux-devtools";

const middleware = [thunk];

export default function configureStore(initialState={}) {
    let enhancer;
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
        enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(
            applyMiddleware(...middleware)
        );
    } else {
        enhancer = compose(
            applyMiddleware(...middleware),
            DevTools.instrument(),
            persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
        );
    }

    return createStore(
        rootReducer,
        initialState,
        enhancer
    );
}