import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import {tpwConfig} from '../config'
import Moment from 'moment';
import Slider from "react-slick";
import { TPW } from './../constants/';
import { IMAGE } from './../constants/image';
import { loadingStart, loadingStop} from './../actions/index';
import $ from 'jquery'

const siteurl = tpwConfig.API_URL;
var HtmlToReactParser = require('html-to-react').Parser;
let sample_image = siteurl + IMAGE.sample_image;

class Single extends Component {
  constructor(props){
    window.scrollTo(0, 0);
    super(props);
    this.state = {
      singleData: [],
      commentData: [],
      postsData: [],
      imageGallery: [],
      morePosts: [],
      pId :'',
      visible: 5,
      error: false,
      progress: true,
      loadComment: false,
      collapseComment: false,
      isLoading: true
    };
    this.loadMore = this.loadMore.bind(this);
    this.loadLess = this.loadLess.bind(this);
    this.callApi = this.callApi.bind(this);
    $(".mp_wrapper .mepr-login-form-wrap").remove();
  }
  loadMore() {
    this.setState({ loadComment: true });
    this.setState((prev) => {
      return {visible: prev.visible + 10};
    });
  }
  loadLess() {
    this.setState({ collapseComment: true });
    this.setState({ loadComment: false });
    this.setState((prev) => {
      return {visible: prev.visible - 10};
    });
  }
  componentDidMount () {
    window.scrollTo(0, 0);
    let that = this;
    fetch(siteurl + TPW.POST_API + that.props.route.match.params.term, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin" })
    .then(function(response) { return response.json(); })
    .then(function(jsonData) {
      return JSON.stringify(jsonData);
    })
    .then(function(jsonStr) {
      let postId = JSON.parse(jsonStr);
      let imgGallery = JSON.parse(jsonStr);
      
      that.setState({
        pId: postId.id,
        imageGallery: imgGallery.acf.image_gallery,
        singleData: JSON.parse(jsonStr),
        isLoading: false,
        progress: false
      });
      that.callApi();
    });
    
  }

  callApi() {
    let that = this;
    fetch(siteurl + TPW.COMMENT_API + that.state.pId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin" })
    .then(function(response) { return response.json(); })
    .then(function(jsonData) {
      return JSON.stringify(jsonData);
    })
    .then(function(jsonStr) {
      that.setState({ commentData: JSON.parse(jsonStr) });
    }).catch(error => {
      this.setState({error: true});
    });

    fetch(siteurl + TPW.MORE_API + that.state.pId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin" })
    .then(function(response) { return response.json(); })
    .then(function(jsonData) {
      return JSON.stringify(jsonData);
    })
    .then(function(jsonStr) {
      that.setState({ morePosts: JSON.parse(jsonStr) });
    }).catch(error => {
      that.setState({error: true});
    });
  }

  createMarkup(authorAvatar){
    return {__html: authorAvatar};
  }

  getTheDuration = () => {
    const { singleData } = this.state;
    Moment.locale('en');
    var dt = singleData.date;
    var startDate = Moment(Moment(dt).format("YYYY-MM-DD[T]HH:mm:ss"));
    var endDate = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
    var days = endDate.diff(startDate, 'days');
    
    if(days > 7 & days < 30 & days < 365){
      var period_of_post = endDate.diff(startDate, 'week');
      var period_suffix = (period_of_post < 1) ? 'Week Ago' : 'Weeks Ago';
    }else if(days > 30 & days < 365){
      var period_of_post = endDate.diff(startDate, 'month');
      var period_suffix = (period_of_post < 1) ? 'Month Ago' : 'Months Ago';
    }else if(days > 365){
      var period_of_post = endDate.diff(startDate, 'year');
      var period_suffix = (period_of_post < 1) ? 'Year Ago' : 'Years Ago';
    }else{
      var period_of_post = days;
      var period_suffix = (days < 1) ? 'Day Ago' : 'Days Ago';
    }
    return period_of_post+" "+period_suffix;    
  }

  onWireView = () => {
    const { singleData, commentData, isLoading } = this.state;
    const { blogLoading } = this.props;

  }

  render() {
    const { singleData, commentData, isLoading, progress } = this.state;
    const { blogLoading, loadingStart, loadingStop } = this.props;
    var ispathName = this.props.route.location.pathname.split('/')[1];
    
    //Get date difference
    Moment.locale('en');
    var info_date = (Moment(singleData.info_date).format("ddd, MMM DD, YYYY"));

    //Get content of the editor
    var content = singleData.content;
    var htmlToReactParser = new HtmlToReactParser();
    var contentElement = htmlToReactParser.parse(content);

    //Get ACF fields
    let parsed = singleData.acf;
    let cat_icon = singleData.category_icon;
    let postFormate = singleData.post_format;

    if( typeof parsed === 'object'){
        if(ispathName !== 'the-spread'){
        var single_featured_image = parsed.single_featured_image.url;
        var imageAlt = parsed.single_featured_image.alt;
        var info_location = parsed.info_location;
        var featured_guest_speaker = parsed.featured_guest_speaker;
        var sponsored_by = parsed.sponsored_by;
        var info_website = parsed.info_website;
        var info_start_time = parsed.info_start_time;
        var info_end_time = parsed.info_end_time;
      }
    }
    if( typeof cat_icon === 'object'){
      var category_icon = cat_icon.url
      var category_icon_alt = cat_icon.alt
    }

    if( typeof postFormate === 'object'){
      var formatName = postFormate[0].name;
    }

    const settings = {
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true
    };
    
    if(progress === true){
      loadingStart();
    }
    if(progress === false){
      loadingStop();
    }

    return (
      <div>
      { ispathName === 'on-the-wire' ?
      <div>
      <section id="post_dtls" className="onthewire">
        <div className="container">
        <div className="inner_postdtls">
          <div className="image_block">
            <div className="imgbox">
              { formatName  === 'Video'?
                <video controls>
                  <source src={singleData.acf ? singleData.acf.video? singleData.acf.video.url:'' : ''} type="video/mp4"></source>
                </video>
                : formatName === false ?
                !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
                : !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
              }
            </div>
          </div>
          <div className="post_title_dtls">
            <div className="blog_dtltitle"><img src={category_icon} alt={category_icon_alt} />{singleData.category_names}</div>
            <h2>{htmlToReactParser.parse(singleData.title)}</h2>
            <div className="blog_author">
              <div className="imgbox"> <img src={ siteurl + IMAGE.trans_50 } alt="" /> <img src={ singleData.author_avatar } className="absoImg" alt="" /> </div>
              <span>{singleData.author_nicename}</span> <span>{this.getTheDuration()}</span> </div>
          </div>
          <div className="body_postdtls">
            {contentElement}
          </div>
          { this.state.commentData.length === 0 ?
            <div className="comments_list">
            <h3 className="nocomments" >No Comments yet. Start a conversation!</h3>
            </div>
            :
          <div className="comments_list">
            <h3>{commentData.length} Comments
            { this.state.loadComment ?
              
              <button onClick={this.loadLess} type="button" className="trans load_less" title="Load More Comments"><i className="fa fa-caret-up" aria-hidden="true"></i></button>
              : this.state.collapseComment ? null
              : ''
            }</h3>
            <ul>
              { this.state.commentData.slice(0, this.state.visible).map((item,index) => { 
                let commentTime = item.date;
                let startTime = Moment(Moment(commentTime).format("YYYY-MM-DD[T]HH:mm:ss"));
                let endTime = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
                let timeResult = Moment.duration(endTime.diff(startTime));
                let hours = timeResult.asHours();
                let hr = parseInt(hours);
                let commentResult, commentPostfix;
                if(hr > 24){ commentResult = parseInt(hr/24); commentPostfix="days" } else{ commentResult = hr; commentPostfix = 'h' }

              return <li key={index}>
                <div className="comments_box">
                  <div className="imgbox"><img src={ siteurl + IMAGE.trans_50 } alt="" />
                  <img className="absoImg" src={item.author_avatar_urls[24]} alt="author_avatar" />
                  </div>
                  <h4>{item.author_name}</h4>
                  <p>{htmlToReactParser.parse(item.content.rendered)}</p>
                  <div className="reply_links"> 
                    <span>{commentResult}{commentPostfix}</span> 
                    {/* <span><a href="#!" className="trans" title="Reply">Reply</a></span>  */}
                  </div>
                </div>
              </li>;
              })
              }
            </ul>
            {this.state.visible < this.state.commentData.length &&
            <div className="load_more"><button onClick={this.loadMore} type="button" className="trans" title="Load More Comments">Load More Comments <i className="fa fa-caret-down" aria-hidden="true"></i></button></div>
            }
            </div>
          }
        </div>
        </div>
      </section>
      <section id="morelove_product">
      <div className="container">
          <div className="inner_morelove">
              <h2 className="title">More to love</h2>
              { this.state.morePosts.length === 0 ?
                <h3 className="nocomments" >No related posts!</h3>
                : <ul>
                  {
                    this.state.morePosts.map((item) => { 
                      return <li key={item.id}>
                  <Link to={`/${ispathName}/${item.slug}`} className="trans" title=""> 
                    <div className="product">
                      <div className="imgbox">
                      { item.post_format === 'video' ?
                        <video controls>
                          <source src={item.acf ? item.acf.video? item.acf.video.url:'' : ''} type="video/mp4"></source>
                        </video>
                        : item.post_format === false ?
                        <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>                      
                        : <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>
                      }
                      </div>
                        <div className="product_detils">
                          <span className="cat_icon"><img src={siteurl + "/src/assets/images/wire_icon.svg"} alt="On the Wire" /></span>
                            <span className="more_view">
                            {/* <ion-icon name="more"></ion-icon> */}
                            </span>
                            <h4>{htmlToReactParser.parse(item.title)}</h4>
                            <div className="lover_block">
                              <div className="imgbox"><img src={siteurl + IMAGE.trans_31 } alt="" /> <img src={item.author_avatar} className="absoImg"  alt="" /></div>
                              <span className="name">{item.author_nicename}</span>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </li>
                    })
                  }
                </ul>
              }
              
            </div>
        </div>
      </section>
      </div>
      : ispathName === 'mark-your-planner' ?
      <div>
      <section id="post_dtls" className="makeyourplanner">
        <div className="container">
          <div className="inner_postdtls">
              <div className="image_block">
              <div className="imgbox">
                <img src={ siteurl + IMAGE.trans_915 } alt="" />
                { !isLoading && <img src={ single_featured_image ? single_featured_image : sample_image } className="absoImg" alt="" /> }
              </div></div>
              <div className="post_title_dtls">
              <div className="blog_dtltitle"><img src={category_icon} alt={category_icon_alt} />{singleData.category_names}</div>
              <h2>{htmlToReactParser.parse(singleData.title)}</h2>
              <div className="blog_author">
                <div className="imgbox"> <img src={ siteurl + IMAGE.trans_50 } alt="" /> 
                <img src={ singleData.author_avatar} className="absoImg" alt="" /> </div>
                <span>{singleData.author_nicename}</span> <span>{this.getTheDuration()}</span> </div>
            </div>
            {/* <div className="postdtls_links">
              <ul>
                <li className="cmnts"><a href="#!" className="trans" title="Comments"><img src="images/comments_gray.png" alt="" />Comments</a></li>
                <li className="save"><a href="#!" className="trans" title="Save"><img src="images/save_gray.svg" alt="" />Save</a></li>
                <li className="share"><a href="#!" className="trans" title="Share"><img src="images/share_icon_gray.svg" alt="" />Share</a></li>
              </ul>
            </div> */}
            <div className="body_postdtls">
              <div className="row">
                <div className="col-md-5 col-sm-12 order-sm-2">
                    <div className="event_info">
                        { info_date && info_start_time && info_end_time ?
                        <div className="event_block">
                          <span className="title">Date & Time</span>
                          <p>{info_date} {info_start_time + '-' + info_end_time}</p>
                        </div>
                        : null }
                        { info_location ?
                        <div className="event_block">
                          <span className="title">Location</span>
                            <p>{info_location}</p>
                        </div>
                        : null }
                        { featured_guest_speaker ?
                        <div className="event_block">
                          <span className="title">Featured Guest and Speakers</span>
                            <p>{featured_guest_speaker}</p>
                        </div>
                        : null} 
                        {sponsored_by ? 
                        <div className="event_block">
                          <span className="title">Sponsored By</span>
                            <p>{sponsored_by}</p>
                        </div>
                        : null }
                        {info_website ?
                        <div className="event_block">
                          <span className="title">Website</span>
                            <p><a href={info_website} className="trans" title={info_website} target="_blank" rel="noopener noreferrer">{info_website}</a></p>
                        </div>
                        : null }
                        { info_date && info_start_time && info_end_time ?
                        <a href="#!" className="trans blue_btn order-sm-1" title="REGISTER">REGISTER</a>
                        : null }                    
                      </div>
                  </div>
                  <div className="col-md-7 col-sm-12 order-sm-1">
                    { contentElement }
                  </div>
                </div>
            </div>
            { this.state.commentData.length === 0 ?
              <div className="comments_list">
              <h3 className="nocomments" >No Comments yet. Start a conversation!</h3>
              </div>
              : <div className="comments_list">
              <h3>{commentData.length} Comments
              {this.state.visible > this.state.commentData.length &&
                <button onClick={this.loadLess} type="button" className="trans load_less" title="Load More Comments"><i className="fa fa-caret-up" aria-hidden="true"></i></button>
              }</h3>
              <ul>
                { this.state.commentData.slice(0, this.state.visible).map((item,index) => { 
                  let commentTime = item.date;
                  let startTime = Moment(Moment(commentTime).format("YYYY-MM-DD[T]HH:mm:ss"));
                  let endTime = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
                  let timeResult = Moment.duration(endTime.diff(startTime));
                  let hours = timeResult.asHours();
                  let hr = parseInt(hours);
                  let commentResult, commentPostfix;
                  if(hr > 24){ commentResult = parseInt(hr/24); commentPostfix="days" } else{ commentResult = hr; commentPostfix = 'h' }

                return <li key={index}>
                  <div className="comments_box">
                    <div className="imgbox">
                    <img src={ siteurl + IMAGE.trans_50 } alt="" /> 
                    <img className="absoImg" src={item.author_avatar_urls[24]} alt="author_avatar" />
                    </div>
                    <h4>{item.author_name}</h4>
                    <p>{htmlToReactParser.parse(item.content.rendered)}</p>
                    <div className="reply_links"> 
                      <span>{commentResult}{commentPostfix}</span> 
                      {/* <span><a href="#!" className="trans" title="Reply">Reply</a></span>  */}
                    </div>
                  </div>
                </li>;
                })
                }
              </ul>
              {this.state.visible < this.state.commentData.length &&
              <div className="load_more"><button onClick={this.loadMore} type="button" className="trans" title="Load More Comments">Load More Comments <i className="fa fa-caret-down" aria-hidden="true"></i></button></div>
              }
              </div>
            }

            {/* <div className="add_cmnts">
              <div className="add_innercmnts">
                <div className="imgbox"><img src="images/trans_50X50.png" alt="" /> <img src="images/auhtor.png" className="absoImg" alt="" /></div>
                <input type="text" className="trans custom_text" title="Add a Comments" placeholder="Add a Comments" />
                <button className="send_btn trans">Send</button>
              </div>
            </div> */}
          </div>
        </div>
      </section>
      <section id="morelove_product">
        <div className="container">
        <div className="inner_morelove">
              <h2 className="title">More to love</h2>
              { this.state.morePosts.length === 0 ?
                <h3 className="nocomments" >No related posts!</h3>
                : <ul>
                  {
                    this.state.morePosts.map((item) => { 
                      return <li key={item.id}>
                  <Link to={`/${ispathName}/${item.slug}`} className="trans" title=""> 
                    <div className="product">
                      <div className="imgbox">
                        <img src={ siteurl + IMAGE.trans_305 } alt="" />
                        <img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" />
                      </div>
                        <div className="product_detils">
                          <span className="cat_icon"><img src={siteurl + "/src/assets/images/plnnner_icon.svg"} alt="Mark Your Planner" /></span>
                            <span className="more_view">
                            {/* <ion-icon name="more"></ion-icon> */}
                            </span>
                            <h4>{htmlToReactParser.parse(item.title)}</h4>
                            <div className="lover_block">
                              <div className="imgbox"><img src={siteurl + IMAGE.trans_31 } alt="" /> <img src={item.author_avatar} className="absoImg"  alt="" /></div>
                              <span className="name">{item.author_nicename}</span>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </li>

                    })
                  }
                </ul>
              }
              
            </div>
          </div>
        </section>
      </div>
      : ispathName === 'the-ultimate-haul' ?
      <div>
      <section id="post_dtls" className="theultimatehau">
        <div className="container">
          <div className="inner_postdtls">
            <strong><div className="image_block">
            <div className="imgbox">
            { formatName  === 'Video'?
              <video controls>
                <source src={singleData.acf ? singleData.acf.video? singleData.acf.video.url:'' : ''} type="video/mp4"></source>
              </video>
              : formatName === false ?
              !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
              : !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
            }
            </div></div></strong>
            <div className="post_title_dtls">
              <div className="blog_dtltitle"><img src={category_icon} alt={category_icon_alt} />{singleData.category_names}</div>
              <h2>{htmlToReactParser.parse(singleData.title)}</h2>
              <div className="blog_author">
                <div className="imgbox"> <img src={ siteurl + IMAGE.trans_50 } alt="" /> <img src={singleData.author_avatar} className="absoImg" alt="" /> </div>
                <span>{singleData.author_nicename}</span> <span>{this.getTheDuration()}</span> </div>
            </div>
            {/* <div className="postdtls_links">
              <a href="#!" title="" ><span className="more_view"><ion-icon name="more"></ion-icon></span></a>
              <ul>
                <li className="cmnts"><a href="#!" className="trans" title="Comments"><img src="images/comments_gray.png" alt="" />Comments</a></li>
                <li className="save"><a href="#!" className="trans" title="Save"><img src="images/save_gray.svg" alt="" />Save</a></li>
                <li className="share"><a href="#!" className="trans" title="Share"><img src="images/share_icon_gray.svg" alt="" />Share</a></li>
              </ul>
            </div> */}
            <div className="see_post">
              <h3 className="title">Shop This Post<i className="fas fa-chevron-down"></i></h3>
              <div className="toggle_slider" >
                <div className="post_slider">
                  <ul>
                    <li>
                      <div className="see_postblock">
                        <div className="imgbox"><img src={ siteurl + IMAGE.trans_70 } alt="" /> <img src={ siteurl + "/src/assets/images/post_img1.png"} className="absoImg" alt="" /></div>
                        <div className="prod_name"><a href="#!" className="trans" title="Pink Planner">Pink Planner</a></div>
                        <div className="prod_price">44.99</div>
                        <div className="shop_link"><a href="#!" className="trans" title="Shop">Shop<i className="fas fa-caret-right"></i></a></div>
                      </div>
                    </li>
                  </ul>
              </div>
              </div>
            </div>
            <div className="body_postdtls">
              { contentElement }
            </div>
            { this.state.commentData.length === 0 ?
          <div className="comments_list">
          <h3 className="nocomments" >No Comments yet. Start a conversation!</h3>
          </div>
          : <div className="comments_list">
          <h3>{commentData.length} Comments
          {this.state.visible > this.state.commentData.length &&
            <button onClick={this.loadLess} type="button" className="trans load_less" title="Load More Comments"><i className="fa fa-caret-up" aria-hidden="true"></i></button>
          }</h3>
          <ul>
            { this.state.commentData.slice(0, this.state.visible).map((item,index) => { 
              let commentTime = item.date;
              let startTime = Moment(Moment(commentTime).format("YYYY-MM-DD[T]HH:mm:ss"));
              let endTime = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
              let timeResult = Moment.duration(endTime.diff(startTime));
              let hours = timeResult.asHours();
              let hr = parseInt(hours);
              let commentResult, commentPostfix;
              if(hr > 24){ commentResult = parseInt(hr/24); commentPostfix="days" } else{ commentResult = hr; commentPostfix = 'h' }
  
            return <li key={index}>
              <div className="comments_box">
                <div className="imgbox">
                  <img src={ siteurl + IMAGE.trans_50} alt="" /> 
                  <img className="absoImg" src={item.author_avatar_urls[24]} alt="author_avatar" />
                </div>
                <h4>{item.author_name}</h4>
                <p>{htmlToReactParser.parse(item.content.rendered.replace)}</p>
                <div className="reply_links"> 
                  <span>{commentResult}{commentPostfix}</span> 
                  {/* <span><a href="#!" className="trans" title="Reply">Reply</a></span>  */}
                </div>
              </div>
            </li>;
            })
            }
          </ul>
          {this.state.visible < this.state.commentData.length &&
          <div className="load_more"><button onClick={this.loadMore} type="button" className="trans" title="Load More Comments">Load More Comments <i className="fa fa-caret-down" aria-hidden="true"></i></button></div>
          }
          </div> 
        }
          </div>
        </div>
      </section>
      <section id="morelove_product">
      <div className="container">

      <div className="inner_morelove">
                <h2 className="title">More to love</h2>
                { this.state.morePosts.length === 0 ?
                <h3 className="nocomments" >No related posts!</h3>
                : <ul>
                  {
                    this.state.morePosts.map((item) => { 
                      return <li key={item.id}>
                  <Link to={`/${ispathName}/${item.slug}`} className="trans" title=""> 
                    <div className="product">
                      <div className="imgbox">
                      { item.post_format === 'video' ?
                        <video controls>
                          <source src={item.acf ? item.acf.video? item.acf.video.url:'' : ''} type="video/mp4"></source>
                        </video>
                        : item.post_format === false ?
                        <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>                      
                        : <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>
                      }
                      </div>
                        <div className="product_detils">
                          <span className="cat_icon"><img src={siteurl + "/src/assets/images/haul_icon.svg"} alt="Ultimate Haul" /></span>
                            <span className="more_view">
                            {/* <ion-icon name="more"></ion-icon> */}
                            </span>
                            <h4>{htmlToReactParser.parse(item.title)}</h4>
                            <div className="lover_block">
                              <div className="imgbox"><img src={siteurl + IMAGE.trans_31 } alt="" /> <img src={item.author_avatar} className="absoImg"  alt="" /></div>
                              <span className="name">{item.author_nicename}</span>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </li>

                    })
                  }
                </ul>
              }
              </div>

      </div>
      </section>
      </div>
      : ispathName === 'plan-connect-inspire' ?
      <div>
      <section id="post_dtls" className="planconceptinspire">
        <div className="container">
          <div className="inner_postdtls">
            <strong><div className="image_block">
            <div className="imgbox">
            { formatName  === 'Video'?
              <video controls>
                <source src={singleData.acf ? singleData.acf.video? singleData.acf.video.url:'' : ''} type="video/mp4"></source>
              </video>
              : formatName === false ?
              !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
              : !isLoading && <span><img src={ siteurl + IMAGE.trans_915 } alt="" /><img className="absoImg" src={ single_featured_image?single_featured_image:sample_image } alt={ imageAlt } /></span>
            }
            </div></div></strong>
            <div className="post_title_dtls">
              <div className="blog_dtltitle"><img src={category_icon} alt={category_icon_alt} />{singleData.category_names}</div>
              <h2>{htmlToReactParser.parse(singleData.title)}</h2>
              <div className="blog_author">
                <div className="imgbox"> <img src={ siteurl + IMAGE.trans_50 } alt="" /> 
                <img src={singleData.author_avatar} className="absoImg" alt="" /> 
              </div>
                <span>{singleData.author_nicename}</span> <span>{this.getTheDuration()}</span> </div>
            </div>
            <div className="body_postdtls">
              { contentElement }
            </div>
            { this.state.commentData.length === 0 ?
          <div className="comments_list">
          <h3 className="nocomments" >No Comments yet. Start a conversation!</h3>
          </div>
          : <div className="comments_list">
          <h3>{commentData.length} Comments
          {this.state.visible > this.state.commentData.length &&
            <button onClick={this.loadLess} type="button" className="trans load_less" title="Load More Comments"><i className="fa fa-caret-up" aria-hidden="true"></i></button>
          }</h3>
          <ul>
            { this.state.commentData.slice(0, this.state.visible).map((item,index) => { 
              let commentTime = item.date;
              let startTime = Moment(Moment(commentTime).format("YYYY-MM-DD[T]HH:mm:ss"));
              let endTime = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
              let timeResult = Moment.duration(endTime.diff(startTime));
              let hours = timeResult.asHours();
              let hr = parseInt(hours);
              let commentResult, commentPostfix;
              if(hr > 24){ commentResult = parseInt(hr/24); commentPostfix="days" } else{ commentResult = hr; commentPostfix = 'h' }
  
            return <li key={index}>
              <div className="comments_box">
                <div className="imgbox">
                  <img src={ siteurl + IMAGE.trans_50} alt="" /> 
                  <img className="absoImg" src={item.author_avatar_urls[24]} alt="author_avatar" />
                </div>
                <h4>{item.author_name}</h4>
                <p>{htmlToReactParser.parse(item.content.rendered.replace)}</p>
                <div className="reply_links"> 
                  <span>{commentResult}{commentPostfix}</span> 
                  {/* <span><a href="#!" className="trans" title="Reply">Reply</a></span>  */}
                </div>
              </div>
            </li>;
            })
            }
          </ul>
          {this.state.visible < this.state.commentData.length &&
          <div className="load_more"><button onClick={this.loadMore} type="button" className="trans" title="Load More Comments">Load More Comments <i className="fa fa-caret-down" aria-hidden="true"></i></button></div>
          }
          </div> 
        }
          </div>
        </div>
      </section>
      <section id="morelove_product">
      <div className="container">
          <div className="inner_morelove">
              <h2 className="title">More to love</h2>
              { this.state.morePosts.length === 0 ?
                <h3 className="nocomments" >No related posts!</h3>
                : <ul>
                  {
                    this.state.morePosts.map((item) => { 
                      return <li key={item.id}>
                  <Link to={`/${ispathName}/${item.slug}`} className="trans" title=""> 
                    <div className="product">
                      <div className="imgbox">
                      { item.post_format === 'video' ?
                        <video controls>
                          <source src={item.acf ? item.acf.video? item.acf.video.url:'' : ''} type="video/mp4"></source>
                        </video>
                        : item.post_format === false ?
                        <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>                      
                        : <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.single_featured_image? item.acf.single_featured_image.url:sample_image:'' } className="absoImg" alt="" /></span>
                      }
                      </div>
                        <div className="product_detils">
                          <span className="cat_icon"><img src={siteurl + "/src/assets/images/plan_inspire_icon.svg"} alt="Plan, Connect, Inspire" /></span>
                            <span className="more_view">
                            {/* <ion-icon name="more"></ion-icon> */}
                            </span>
                            <h4>{htmlToReactParser.parse(item.title)}</h4>
                            <div className="lover_block">
                              <div className="imgbox"><img src={siteurl + IMAGE.trans_31 } alt="" /> <img src={item.author_avatar} className="absoImg"  alt="" /></div>
                              <span className="name">{item.author_nicename}</span>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </li>

                    })
                  }
                </ul>
              }
            </div>
        </div>
      </section>
      </div>
      : ispathName === 'the-spread' ?
      <div>
      <section id="post_dtls" className="thespread">
        <div className="container">
          <div className="inner_postdtls">
            <div className="image_block sliders">
            { formatName  === 'Video'?
              <video controls>
                <source src={singleData.acf ? singleData.acf.video? singleData.acf.video.url:'' : ''} type="video/mp4"></source>
              </video>
              : formatName === false ?
              this.state.imageGallery.length > 0 ? 
            <ul>
            <Slider {...settings}>
              { this.state.imageGallery.map((item) => {
              return <li>
                <div className="imgbox">
                  <img src={ siteurl + IMAGE.trans_915 } alt="" />
                  { !isLoading && <img src={item.image.url} className="absoImg" alt="" /> }
                </div>
              </li>
              }) }
            </Slider>
            </ul>
            : <div className="imgbox">
              <img src={ siteurl + IMAGE.trans_915 } alt="" />
              <img src={sample_image} className="absoImg" alt="sampleImage" /></div>
              : this.state.imageGallery.length > 0 ? 
              <ul>
              <Slider {...settings}>
                { this.state.imageGallery.map((item) => {
                return <li>
                  <div className="imgbox">
                    <img src={ siteurl + IMAGE.trans_915 } alt="" />
                    { !isLoading && <img src={item.image.url} className="absoImg" alt="" /> }
                  </div>
                </li>
                }) }
              </Slider>
              </ul>
              : <div className="imgbox">
                <img src={ siteurl + IMAGE.trans_915 } alt="" />
                <img src={sample_image} className="absoImg" alt="sampleImage" /></div>
            }
            </div>
            <div className="post_title_dtls">
              <div className="blog_dtltitle"><img src={category_icon} alt={category_icon_alt} />{singleData.category_names}</div>
              <h2>{htmlToReactParser.parse(singleData.title)}</h2>
              <div className="blog_author">
                <div className="imgbox"> <img src={ siteurl + IMAGE.trans_50 } alt="" /> <img src={singleData.author_avatar} className="absoImg" alt="" /> </div>
                <span>{singleData.author_nicename}</span> <span>{this.getTheDuration()}</span> </div>
            </div>
            <div className="body_postdtls">
              { contentElement }
            </div>
            { this.state.commentData.length === 0 ?
          <div className="comments_list">
          <h3 className="nocomments" >No Comments yet. Start a conversation!</h3>
          </div>
          : <div className="comments_list">
          <h3>{commentData.length} Comments
          {this.state.visible > this.state.commentData.length &&
            <button onClick={this.loadLess} type="button" className="trans load_less" title="Load More Comments"><i className="fa fa-caret-up" aria-hidden="true"></i></button>
          }</h3>
          <ul>
            { this.state.commentData.slice(0, this.state.visible).map((item,index) => { 
              let commentTime = item.date;
              let startTime = Moment(Moment(commentTime).format("YYYY-MM-DD[T]HH:mm:ss"));
              let endTime = Moment(Moment().format("YYYY-MM-DD[T]HH:mm:ss"));
              let timeResult = Moment.duration(endTime.diff(startTime));
              let hours = timeResult.asHours();
              let hr = parseInt(hours);
              let commentResult, commentPostfix;
              if(hr > 24){ commentResult = parseInt(hr/24); commentPostfix="days" } else{ commentResult = hr; commentPostfix = 'h' }
  
            return <li key={index}>
              <div className="comments_box">
                <div className="imgbox">
                  <img src={ siteurl + IMAGE.trans_50 } alt="" /> 
                  <img className="absoImg" src={item.author_avatar_urls[24]} alt="author_avatar" />
                </div>
                <h4>{item.author_name}</h4>
                <p>{htmlToReactParser.parse(item.content.rendered.replace)}</p>
                <div className="reply_links"> 
                  <span>{commentResult}{commentPostfix}</span> 
                  {/* <span><a href="#!" className="trans" title="Reply">Reply</a></span>  */}
                </div>
              </div>
            </li>;
            })
            }
          </ul>
          {this.state.visible < this.state.commentData.length &&
          <div className="load_more"><button onClick={this.loadMore} type="button" className="trans" title="Load More Comments">Load More Comments <i className="fa fa-caret-down" aria-hidden="true"></i></button></div>
          }
          </div> 
        }
          </div>
        </div>
      </section>
      <section id="morelove_product">
      <div className="container">
          <div className="inner_morelove">
              <h2 className="title">More to love</h2>
              { this.state.morePosts.length === 0 ?
                <h3 className="nocomments" >No related posts!</h3>
                : <ul>
                  {
                    this.state.morePosts.map((item) => { 
                      return <li key={item.id}>
                  <Link to={`/${ispathName}/${item.slug}`} className="trans" title=""> 
                    <div className="product">
                      <div className="imgbox">
                      { item.post_format === 'video' ?
                        <video controls>
                          <source src={item.acf ? item.acf.video? item.acf.video.url:'' : ''} type="video/mp4"></source>
                        </video>
                        : item.post_format === false ?
                        <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.image_gallery[0]? item.acf.image_gallery[0].image.url:sample_image:'' } className="absoImg" alt={item.acf ? item.acf.image_gallery? item.acf.image_gallery[0].image.alt:'sample_image':''} /></span>
                        : <span><img src={ siteurl + IMAGE.trans_305 } alt="" /><img src={ item.acf ? item.acf.image_gallery[0]? item.acf.image_gallery[0].image.url:sample_image:'' } className="absoImg" alt={item.acf ? item.acf.image_gallery? item.acf.image_gallery[0].image.alt:'sample_image':''} /></span>
                      }                        
                      </div>
                        <div className="product_detils">
                          <span className="cat_icon"><img src={siteurl + "/src/assets/images/spread.svg"} alt="The Spread" /></span>
                            <span className="more_view">
                            {/* <ion-icon name="more"></ion-icon> */}
                            </span>
                            <h4>{htmlToReactParser.parse(item.title)}</h4>
                            <div className="lover_block">
                              <div className="imgbox"><img src={siteurl + IMAGE.trans_31 } alt="" /> <img src={item.author_avatar} className="absoImg"  alt="" /></div>
                              <span className="name">{item.author_nicename}</span>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </li>

                    })
                  }
                </ul>
              }
            </div>
        </div>
      </section>
      </div>
      : <section id="blog"> Nothing Found</section>
      }
      </div>
    );
  }
}

//export default Single;
const mapStateToProps = state => ({
  blogLoading: state.blog.blogLoading,
})

const mapDispatchToProps = dispatch => ({
  loadingStart: () => { dispatch(loadingStart()) },
  loadingStop: () => { dispatch(loadingStop()) },
})
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Single));