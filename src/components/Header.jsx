import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { tpwConfig } from "../config";
import TopMenu from "./parts/TopMenu";
import Search from "./searchComponent/searchForm";
import isEmpty from 'lodash/isEmpty';
import $ from "jquery";
import { IMAGE } from "./../constants/image";
import { TPW } from "./../constants/index";

const siteurl = tpwConfig.API_URL;
const API = TPW.ACF_API;

class Header extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      acfData: [],
      username: null
    };

  }
  componentDidMount() {
    $(".search_header .searchbar a").click(function(e) {
      $(".search_desk").slideToggle();
      $(".custom_nav .inner_navbar").toggleClass("search_open");
    });
    $(".search_desk  .close_search").click(function(e) {
      $(".search_desk").slideUp();
      $(".custom_nav .inner_navbar").removeClass("search_open");
    });

    const that = this;
    fetch(siteurl + API, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    })
      .then(function(response) {
        return response.json();
      })
      .then(function(jsonData) {
        return JSON.stringify(jsonData);
      })
      .then(function(jsonStr) {
        that.setState({ acfData: JSON.parse(jsonStr) });
      });

      this.setState({
        username: this.props.route.match.params.username
      })
  }

  /**Search**/
  handleSearch = () => {};

  render() {
    const { acfData, username } = this.state;
    const { menuData, userData } = this.props;
    
    let logoURL = typeof acfData.sitelogo != 'undefined' &&  !isEmpty(acfData.sitelogo.url) 
                  ? acfData.sitelogo.url : '';
    let pathName = window.location.pathname.split("/")[3];
       
    return (
      <header id="header">
        <div className="top_header">
          <div className="container">
            <div className="inner_topheader">
              <h1>{acfData.sitetagline}</h1>
            </div>
          </div>
        </div>
        <div className="main_header">
          <div className="container">
            <div className="inner_mainheader">
              <div className="leftbar_header">
                <div className="user_login">
                  <a href="#!">
                    <div className="imgbox">
                      <img src={siteurl + IMAGE.trans_31} alt="" />
                      {userData.isAuthenticated === true ? (
                        <img
                          src={userData.tpwUser.user_avatar}
                          className="absoImg"
                          alt=""
                        />
                      ) : (
                        <img
                          src={siteurl + IMAGE.defaultAvatar}
                          className="absoImg"
                          alt=""
                        />
                      )}
                    </div>
                  </a>
                  <div className="user_list">
                    <div className="user_profile">
                      {" "}
                      <a href="#!">
                        <div className="imgbox">
                          {" "}
                          <img src={siteurl + IMAGE.trans_31} alt="" />
                          {userData.isAuthenticated === true ? (
                            <img
                              src={userData.tpwUser.user_avatar}
                              className="absoImg"
                              alt=""
                            />
                          ) : (
                            <img
                              src={siteurl + IMAGE.defaultAvatar}
                              className="absoImg"
                              alt=""
                            />
                          )}
                        </div>
                        <span className="user_name">
                          {userData.isAuthenticated === true
                            ? userData.tpwUser.user_display_name
                            : " "}
                        </span>
                      </a>
                    </div>
                    <ul>
                      <li>
                        <a
                          href={siteurl}
                          className="trans"
                          title="Create A Post"
                        >
                          <img
                            src={siteurl + "/src/assets/images/pencil.svg"}
                            alt=""
                          />
                          Create A Post
                        </a>
                      </li>
                      <li>
                        <a href={siteurl} className="trans" title="Favorites">
                          <img
                            src={siteurl + "/src/assets/images/star_icon.svg"}
                            alt=""
                          />
                          Favorites
                        </a>
                      </li>
                      <li>
                        <a href={siteurl} className="trans" title="Saved Posts">
                          <img
                            src={siteurl + "/src/assets/images/post_icon.svg"}
                            alt=""
                          />
                          Saved Posts
                        </a>
                      </li>
                      <li>
                        <a href={siteurl} className="trans" title="Messages">
                          <img
                            src={siteurl + "/src/assets/images/msg.svg"}
                            alt=""
                          />
                          Messages
                        </a>
                      </li>
                    </ul>
                    <div className="user_set">
                      <ul>
                        {userData.isAuthenticated === true ? (
                          <li>
                            <a
                              href={siteurl + "/user/signup/account"}
                              className="trans"
                              title="Account"
                            >
                              Account
                            </a>
                          </li>
                        ) : (
                          <li>
                            <a
                              href={siteurl + "/user/signup"}
                              className="trans"
                              title="Account"
                            >
                              Account
                            </a>
                          </li>
                        )}
                        <li>
                          <a href={siteurl} className="trans" title="Settings">
                            Settings
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              
              {!isEmpty(logoURL) ? 
                <div className="logo">
                  {" "}
                  <Link to="" className="" title="ThePannerWire">
                    <img src={logoURL} alt="ThePannerWire" />
                  </Link>{" "}
                </div>
              : ''}

              <div id="mobile-menu-toggle">
                {" "}
                <span /> <span /> <span /> <span />{" "}
              </div>
              <div className="search_header">
                <div className="searchbar">
                  {" "}
                  <a
                    onClick={this.handleSearch}
                    className="trans"
                    title="Search"
                  >
                    Search<i className="fa fa-search" aria-hidden="true" />
                  </a>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
        {pathName !== "saved-posts" ? (
          pathName !== "favorites" ? (
            <div className="custom_nav">
              <div className="container">
                <TopMenu menuItem={menuData} />
              </div>
            </div>
          ) : null
        ) : null}
        <div className="search_desk">
          <Search />
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    userData: state.authentication,
    acfData: state.topMenu.acfData
  };
};

export default connect(
  mapStateToProps,
  null
)(Header);
