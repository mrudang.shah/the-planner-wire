import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {tpwConfig} from '../../config'

const siteurl = tpwConfig.API_URL;
const API = '/wp/wp-json/better-rest-endpoints/v1/menus/footer-menu';

class FooterMenu extends Component {
  constructor(props){
    super(props);
    this.state = {
      ftrData: []
    };
  }
  componentDidMount () {
    fetch(siteurl + API, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin" })
      .then(
      response => response.json()
      ).then(
      myData => {
        this.setState({ ftrData: myData });
      }
      ).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render() {
    return this.state.ftrData.map((item,index) => { 
      return (
        <ul key={index}>
          <li className={item.classes}>
            <Link to={item.slug}>{item.title}</Link>
          </li>
        </ul>
      )
    });
  }
}

export default FooterMenu;
