import React, { Component } from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import './assets/css/all.css';
import './assets/css/fontawesome.min.css';
import './assets/css/bootstrap.min.css';
import './assets/css/slick.css';
import './assets/css/docs.css';
import './assets/js/slick.js';
import './assets/css/custom.css';

import { custom } from '../src/assets/js/custom';
import { topMenu, getPost } from './actions/topMenuAction';

import DefaultLayout  from './layouts/defaultLayout';
import WithoutMenu  from './layouts/blankPageLayout';

import defaultRoutes  from './routes/defaultRoutes';
import sessionRoutes  from './routes/sessionRoutes';
import { history } from './helpers';

import _ from 'lodash';

class App extends Component {
  constructor(props){
    super(props);
    this.state = { };
  }
  
  componentWillMount() {
    const { topMenu, getPost } = this.props
    topMenu();
    getPost();
  }

  componentDidMount() {
    custom();
  }

  render() {
    const { topMenuData } = this.props

    return (
      <BrowserRouter>
          <div>
            { _.map(defaultRoutes, (route, key) => {
                    const { component, path } = route;
                    return (
                        <Route
                            history = {history}
                            exact
                            path={path}
                            key={key}
                            render={ (route) => <DefaultLayout component={component} route={route} menuData={topMenuData}/>}
                        />
                    )
                }) }

            { _.map(sessionRoutes, (route, key) => {
                    const { component, path } = route;
                    return (
                        <Route
                            history = {history}
                            exact
                            path={path}
                            key={key}
                            render={ (route) => <WithoutMenu component={component} route={route}/>}
                        />
                    )
                }) }    
          </div>

      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
  topMenuData: state.topMenu.topMenuData ,
  postData: state.topMenu.postData ,
	topMenuLoading: state.topMenu.topMenuData 
});

const mapDispatchToProps = dispatch => ({
  topMenu: () => { dispatch(topMenu()) },
  getPost: () => { dispatch(getPost()) }
})


export default connect(mapStateToProps, mapDispatchToProps)(App);