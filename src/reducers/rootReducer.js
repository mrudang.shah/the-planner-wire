import { combineReducers } from 'redux';
import topMenu from './topMenuReducer';
import user from './user/userReducer';
import favouriteData from './favouriteReducer';
import memberData from './memberReducer';
import { authentication } from './authenticationReducer';
import signupResponse from './user/signupReducer';
import AccountReducer from './user/accountReducer';
import blog from './blogReducer';
import page from './index';

export default combineReducers({
    topMenu,
    user,
    favouriteData,
    memberData,
    authentication,
    signup: signupResponse,
    accountInfo: AccountReducer,
    blog,
    page
});