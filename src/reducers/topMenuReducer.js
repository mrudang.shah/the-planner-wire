const initialState = {
	topMenuData: [],
	topMenuLoading: true,
	postData: [],
	acfData: [],
};


export const topMenu = (state = initialState, action = {}) => {
  switch(action.type) {
    case "TOPMENU_LOADING_START":
			return {
				...state,
				topMenuLoading: true
			}

		case "TOPMENU_LOADING_DONE":
			return {
				...state,
				topMenuLoading: false
			}

		case "SET_TOP_MENU":
			return {
				...state,
				topMenuData: action.topMenuData
      }
			
		case "SET_POST_DATA":
			return {
				...state,
				postData: action.postData
			}
		
		case "SET_ACF_DATA":
			return{
				...state,
				acfData: action.acfData
			}
			
    default: return state  
  }
}

export default topMenu;