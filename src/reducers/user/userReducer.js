const initialState = {
	userId: null,
	userLoading: true
};


export const user = (state = initialState, action = {}) => {
  switch(action.type) {
		case "USER_LOADING_START":
			return {
				...state,
				userLoading: true
			}

		case "USER_LOADING_DONE":
			return {
				...state,
				userLoading: false
			}

    case "GET_USER_ID":
			return {
				...state,
				userId: action.userId
      }
		
		default: return state  
  }
}

export default user;